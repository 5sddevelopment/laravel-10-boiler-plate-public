<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthenticationRequest;
use App\Interfaces\IUserRepository;
use App\Mail\RegisterationOTP;
use App\Mail\VerifyOTP;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Laravel\Passport\Bridge\UserRepository;

class AuthenticationController extends Controller
{
    private IUserRepository $userRepository;
    private bool $VerifyEmail;

    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->VerifyEmail = (bool)$this->GetConfiguration('API Email Verification');
    }

    public function register(AuthenticationRequest $request)
    {
        try {
            if (Mail::to('ehtisham.hussain@code-avenue.com')->send(new VerifyOTP('1234'))) {
                
            }
        } catch (Exception $exception) {
            return $exception;
        }
    }
}
