<?php

namespace App\Http\Controllers;

use App\Models\Configuration;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    protected function GetConfiguration($config_key)
    {
        return Configuration::where('config_key', $config_key)->first()->config_value;
    }
}
