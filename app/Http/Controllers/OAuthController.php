<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\FacebookProvider;
use Laravel\Socialite\Two\GithubProvider;
use Laravel\Socialite\Two\GoogleProvider;
use Laravel\Socialite\Two\TwitterProvider;

class OAuthController extends Controller
{

    private function getProvider($providerName)
    {
        $config = [
            'client_id' => env(strtoupper($providerName) . '_CLIENT_ID'),
            'client_secret' => env(strtoupper($providerName) . '_CLIENT_SECRET'),
            'redirect' => env('APP_URL') . '/oauth/callback/' . $providerName
        ];

        switch ($providerName) {
            case "google":
                return Socialite::buildProvider(GoogleProvider::class, $config);
            case "facebook":
                return Socialite::buildProvider(FacebookProvider::class, $config);
            case "twitter":
                return Socialite::buildProvider(TwitterProvider::class, $config);
            case "github":
                return Socialite::buildProvider(GithubProvider::class, $config);
        }
    }

    public function redirectToProvider($providerName)
    {
        return $this->getProvider($providerName)->redirect();
    }

    public function handleOAuthCallback($providerName)
    {
        dd($this->getProvider($providerName)->user());
        $user = $this->getProvider($providerName)->user();
    }
}
