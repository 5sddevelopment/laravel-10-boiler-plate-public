<?php

namespace App\Interfaces;

interface IUserRepository extends BaseInterface
{
    public function create(array $params);
}
