<?php

namespace App\Models;

use Database\Seeders\OAuthProviderSeeder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OAuthProvider extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
    ];
}
