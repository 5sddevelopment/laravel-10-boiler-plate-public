<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AccountStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('account_statuses')->insert([
            ['id' => 1, 'name' => 'active',],
            ['id' => 2, 'name' => 'suspended',],
            ['id' => 3, 'name' => 'blocked',],
        ]);
    }
}
