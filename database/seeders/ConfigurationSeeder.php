<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {

        $now = Carbon::now();
        DB::table('configurations')->insert([
            ['id' => 1, 'config_key' => 'API Email Verification', 'config_value' => 'ON', 'description' => 'API Email verification for account registration', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
