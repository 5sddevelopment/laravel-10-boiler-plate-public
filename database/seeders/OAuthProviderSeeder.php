<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OAuthProviderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $now = Carbon::now();
        DB::table('oauth_providers')->insert([
            ['id' => 1, 'name' => 'google', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2, 'name' => 'facebook', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 3, 'name' => 'twitter', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
