<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OTPTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $now = Carbon::now();
        DB::table('otp_types')->insert([
            ['id' => 1, 'name' => 'User Registration', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 2, 'name' => 'Password Recovery', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 3, 'name' => 'Deactivate Account', 'created_at' => $now, 'updated_at' => $now],
            ['id' => 4, 'name' => 'Activate Account', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
