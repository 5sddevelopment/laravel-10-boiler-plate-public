@component('mail::message')
    {{ __('The verification code is :code ', ['code' => $code]) }}

    {{ __('If you did not expect to receive an invitation to this team, you may discard this email.') }}
@endcomponent
